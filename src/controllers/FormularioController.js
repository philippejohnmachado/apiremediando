'use strict';

const mongoose = require('mongoose');
const MensagemModel = require('../models/mensagemClienteModel');
const emailservice = require('../services/email-services');
const repository = require('../repositories/mensagemClienteRepository');

exports.MensagemCliente = async(req, res, next)=>{
    try{
        await repository.creat(req.body);

        emailservice.send(
            req.body.email,
            'Masllow',
             global.EMAIL_MSN.replace('{0}',req.body.name)
             );

        res.status(200).send({
            message: 'Mensagem cadastrada',
            result: true,
            error: false
        });
    }catch(e){
        res.status(500).send({
            message: 'Falha ao processar  sua informação.',
            result: false,
            error: true
        });
    }
};