'use  strict';

const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const authService = require('../services/auth-services');

router.get('/:email',authService.autorize, userController.get);
router.get('/novasenha/:email', userController.NovaSenha);
router.post('/', userController.post);
router.post('/validation', userController.UserValidacao);
router.put('/:id',authService.autorize, userController.put);
router.post('/novaSenha',authService.autorize, userController.UpdateNovaSenha);
// router.delete('/', Controller.post);

module.exports = router;