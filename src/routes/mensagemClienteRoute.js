'use  strict';

const express = require('express');
const router = express.Router();
const FormulariController = require('../controllers/FormularioController');
const authService = require('../services/auth-services');

router.post('/', FormulariController.MensagemCliente );

module.exports = router;