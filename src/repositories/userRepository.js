'use strict';

const mongoose =  require('mongoose');
const user = mongoose.model('user');
const userModel = require('../models/userModel');

exports.get = async(email)=>{
    const res = await user.findOne({
        email: email
    });
    return res;
}

exports.creat = async(data) =>{
    var user = new userModel(data);
    await user.save();
}

exports.updateSenhaProvisoria = async(id,Senha) =>{
  await user.findByIdAndUpdate(id,{
        $set:{
            senha: Senha,
            senhaProvi: true
        }
    })
}

exports.updateNovaSenha = async(data) =>{
    await user.findByIdAndUpdate(data.UserId,{
          $set:{
              senha: data.senha,
              senhaProvi: false
          }
      })
  }