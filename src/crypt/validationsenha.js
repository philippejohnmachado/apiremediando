'use  strict';

var CryptoJS = require("crypto-js");

exports.decrypt = async(senha)=>{
    var bytes  = await CryptoJS.AES.decrypt(senha.toString(),global.SALT_KEY);
    var plaintext = await bytes.toString(CryptoJS.enc.Utf8);
    return plaintext;
}

exports.crypt = async(senha)=>{
    var ciphertext = await CryptoJS.AES.encrypt(senha.toString(),global.SALT_KEY);
    return ciphertext;
}