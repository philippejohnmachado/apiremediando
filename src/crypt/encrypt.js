'use  strict';

var CryptoJS = require("crypto-js");

exports.crypt = async(senha)=>{
    var ciphertext = await CryptoJS.AES.encrypt(senha.toString(),global.SALT_KEY);
    return ciphertext;
}