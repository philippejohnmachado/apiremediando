'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const sch= new Schema({
    nome:{
        type: String,
        required: true,
        trim: true
    },
    sobrenome:{
        type: String,
        required: true,
        trim: true
    },
    email:{
        type: String,
        required: true,
        trim: true
    },
    cpf:{
        type: String,
        required: true,
        trim: true
    },
    senha:{
        type: String,
        required: true,
        trim: true
    },
    senhaProvi:{
        type: Boolean,
        required: true,
        default: false
    }
});

module.exports = mongoose.model('user', sch);