'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const sch= new Schema({
    EAN:{
        type: String,
        required: true,
        trim: true
    },
    NomeProduto:{
        type: String,
        required: true,
        trim: true
    },
    DescricaoProduto:{
        type: String,
        required: true,
        trim: true
    },
    PrecoDe:{
        type: String,
        required: true,
        trim: true
    },
    PrecoPor:{
        type: String,
        required: true,
        trim: true
    },
    VendidoPor:{
        type: String,
        required: true,
        trim: true
    },
    Fabricante:{
        type: String,
        required: true,
        trim: true
    },
    TipoMedicamento:{
        type: String,
        required: true,
        trim: true
    },
    ComReceita:{
        type: String,
        required: true,
        trim: true
    },
    ParaQueServe:{
        type: String,
        required: true,
        trim: true
    },
    RegistroMS:{
        type: String,
        required: true,
        trim: true
    },
    CodigoInterno:{
        type: String,
        required: false,
        trim: true
    },
    PrincipioAtivo:{
        type: String,
        required: true,
        trim: true
    },
    ClasseTerapeutica:{
        type: String,
        required: true,
        trim: true
    },
    CategoriaMedicamento:{
        type: String,
        required: true,
        trim: true
    },
    Especialidades:{
        type: String,
        required: true,
        trim: true
    }
});

module.exports = mongoose.model('produtos', sch);